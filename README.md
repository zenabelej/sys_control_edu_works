# Дипломний проєкт

```shell
cp .env.example .env
```

Configuration connect to database
```dotenv
DB_CONNECTION=pgsql
DB_HOST=db
DB_PORT=5432
DB_DATABASE=edu_work
DB_USERNAME=user
DB_PASSWORD=pass
```

Up docker container:
```shell
docker compose up -d
```

---

### I recommend making an alias:
```shell
alias p='docker exec -it php'
```

---

Update composer
```shell
p composer update
```
Install composer
```shell
p composer install
```
Generate key:
```shell
p php artisan key:generate
```

Migrate database:
```shell
p php artisan migrate --seed
```
NPM
```shell
p npm install
```

```shell
p npm run build
```

Create admin user in Laravel Orchid:
```shell
p php artisan orchid:admin admin admin@admin.com password
```

If there are problems with Laravel Orcid, save the ``app/Models/User.php`` file and execute the command:
```shell
p php artisan orchid:install
```

In order to stop
```shell
docker compose down
```
