<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Index', [
        'user' => auth('student')->user(),
        'canLogin' => auth('student')->check(),
    ]);
})->name('home');

Route::get('/ini', function () {
    return phpinfo();
});

Route::middleware('auth:student')->group(function () {
    Route::get('page', [StudentController::class, 'show'])->name('userPage');

    Route::get('apply/{project}', [NotificationController::class, 'notificationApply'])->name('notification.apply');

    Route::get('projects', [ProjectController::class, 'index'])->name('projects.index');
    Route::get('projects/{project}', [ProjectController::class, 'show'])->name('projects.show');
});

Route::prefix('auth')->group(function () {
    Route::get('/', [AuthController::class, 'signIn'])->name('auth');

    Route::get('google', [SocialController::class, 'redirectToGoogle'])->name('auth.google');
    Route::get('google/callback', [SocialController::class, 'handleGoogleCallback']);

    Route::get('github', [SocialController::class, 'redirectToGitHub'])->name('auth.github');
    Route::get('github/callback', [SocialController::class, 'handleGitHubCallback']);

    Route::get('gitlab', [SocialController::class, 'redirectToGitLab'])->name('auth.gitlab');
    Route::get('gitlab/callback', [SocialController::class, 'handleGitLabCallback']);
});

Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::fallback(function () {
    return Inertia::render('404');
});
