<?php

use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    Route::get('students', [StudentController::class, 'index']);
    Route::get('projects', [ProjectController::class, 'index'])->name('api.projects.index');
});
