<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            'Легка',
            'Середня',
            'Важко',
            'Гнучко',
        ];

        $inserts = array_map(function ($state) {
            return [
                'state' => $state,
            ];
        }, $states);

        State::insert($inserts);
    }
}
