<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamp('start')->useCurrent();
            $table->unsignedBigInteger('event_id');
            $table->timestamp('end')->useCurrent();
            $table->unsignedInteger('state_id');
            $table->unsignedInteger('students_id')->nullable();
            $table->string('brief_description', 250)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('students_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
