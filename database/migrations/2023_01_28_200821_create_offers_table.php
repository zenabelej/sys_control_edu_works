<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->unsignedBigInteger('teacher_id');
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('student_id');

            $table->timestamps();

            $table->primary([
                'teacher_id',
                'project_id',
            ]);

            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
};
