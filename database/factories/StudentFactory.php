<?php

namespace Database\Factories;

use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Student>
 */
class StudentFactory extends Factory
{

    protected $model = Student::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nickname' => $this->faker->name,
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'avatar' => $this->faker->imageUrl,
            'google_id' => $this->faker->ean8(),
            'gitlab_id' => $this->faker->ean8(),
            'github_id' => $this->faker->ean8(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
