<?php

namespace App\Orchid\Layouts\Technologys;

use App\Models\Technology;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TechnologysListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'technology';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('title', __('admin.name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(fn(Technology $technology) => Link::make($technology->title)
                    ->route('platform.technologys.edit', $technology->id)),
            TD::make('created_at', __('admin.created'))
                ->sort()
                ->render(fn(Technology $technology) => $technology->created_at),
            TD::make('updated_at', __('admin.updated'))
                ->sort()
                ->render(fn(Technology $technology) => $technology->updated_at),
        ];
    }
}
