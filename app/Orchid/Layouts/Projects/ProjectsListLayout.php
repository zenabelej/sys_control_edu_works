<?php

namespace App\Orchid\Layouts\Projects;

use App\Models\Project;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ProjectsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'project';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('title', __('admin.name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(fn(Project $project) => Link::make($project->title)
                    ->route('platform.projects.project', $project->id)),
            TD::make('event_id', __('admin.project_event'))
                ->filter(TD::FILTER_DATE_RANGE)
                ->sort()
                ->render(fn(Project $project) => $project->event->title),
            TD::make('brief_description', __('admin.project_brief_description'))
                ->sort(),
            TD::make('students_id', __('admin.project_students_id'))
                ->sort()
                ->filter()
                ->render(fn(Project $project) => $project->student?->name),
            TD::make('created_at', __('admin.created'))
                ->sort()
                ->render(fn(Project $project) => $project->created_at)
                ->defaultHidden(),
            TD::make('updated_at', __('admin.updated'))
                ->sort()
                ->render(fn(Project $project) => $project->updated_at)
                ->defaultHidden(),
            TD::make('')->render(
                fn(Project $project) => DropDown::make()
                    ->icon('options-vertical')
                    ->list([
                        Link::make(__('admin.review'))
                            ->icon('eye')
                            ->route('platform.projects.project', $project->id),
                        Link::make(__('Edit'))
                            ->route('platform.projects.edit', $project->id)
                            ->icon('pencil'),
                        Button::make(__('Delete'))
                            ->method('remove')
                            ->icon('trash')
                            ->confirm(__('admin.project_are_you_sure?'))
                            ->parameters([
                                'project' => $project->id,
                            ]),
                    ]),
            )
                ->cantHide()
                ->alignRight(),

        ];
    }
}
