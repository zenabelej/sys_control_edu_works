<?php

namespace App\Orchid\Layouts\Projects;

use App\Models\Event;
use App\Models\State;
use App\Models\Student;
use App\Models\Technology;
use Illuminate\Contracts\Container\BindingResolutionException;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Throwable;

class ProjectsEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     * @throws BindingResolutionException
     * @throws Throwable
     */
    protected function fields(): iterable
    {
        return [
            Input::make('project.title')
                ->maxlength(255)
                ->required()
                ->title(__('admin.project_input_name'))
                ->placeholder(__('admin.name')),

            Relation::make('project.event_id')
                ->fromModel(Event::class, 'title')
                ->title(__('admin.project_input_event'))
                ->required()
                ->placeholder(__('admin.project_placeholder_input_event')),

            DateRange::make('project.event')
                ->title(__('admin.project_date_range_input_start_end')),

            Relation::make('project.state_id')
                ->fromModel(State::class, 'state')
                ->title(__('admin.project_input_state'))
                ->required(),

            Relation::make('project.technologys.')
                ->fromModel(Technology::class, 'title')
                ->title(__('admin.project_input_technology'))
                ->chunk(PHP_INT_MAX)
                ->multiple(),

            Relation::make('project.students_id')
                ->fromModel(Student::class, 'name')
                ->title(__('admin.project_input_student')),

            TextArea::make('project.brief_description')
                ->maxlength(250)
                ->title(__('admin.project_input_brief_description'))
                ->help(__('admin.project_input_help_brief_description')),

            Quill::make('project.description')
                ->title(__('admin.description')),

            Upload::make('project.attachment')
                ->title(__('admin.project_input_upload'))
                ->storage('public'),

        ];
    }
}
