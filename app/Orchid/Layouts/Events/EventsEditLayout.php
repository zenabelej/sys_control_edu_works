<?php

namespace App\Orchid\Layouts\Events;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class EventsEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('event.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('admin.event_input_name'))
                ->placeholder(__('admin.name')),
            TextArea::make('event.description')
                ->title(__('admin.event_input_description'))
                ->placeholder(__('admin.description'))
                ->max(255),
        ];
    }
}
