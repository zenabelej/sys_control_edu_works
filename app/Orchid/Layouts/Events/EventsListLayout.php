<?php

namespace App\Orchid\Layouts\Events;

use App\Models\Event;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class EventsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'event';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('title', __('admin.name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(fn(Event $event) => Link::make($event->title)
                    ->route('platform.events.edit', $event->id)),
            TD::make('description', __('admin.description'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(fn(Event $event) => Link::make($event->description)
                    ->route('platform.events.edit', $event->id)),
            TD::make('created_at', __('admin.created'))
                ->sort()
                ->render(fn(Event $event) => $event->created_at),
            TD::make('updated_at', __('admin.updated'))
                ->sort()
                ->render(fn(Event $event) => $event->updated_at),
        ];
    }
}
