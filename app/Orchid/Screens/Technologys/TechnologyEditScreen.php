<?php

namespace App\Orchid\Screens\Technologys;

use App\Models\Technology;
use App\Orchid\Layouts\Technologys\TechnologyEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TechnologyEditScreen extends Screen
{
    public Technology $technology;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Technology $technology): iterable
    {
        return [
            'technology' => $technology,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return __('admin.technology_title');
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('admin.save'))
                ->icon('check')
                ->method('save'),
            Button::make(__('admin.delete'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->technology->exists),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(TechnologyEditLayout::class)
                ->title(__('admin.technology_block_name'))
                ->description(__('admin.technology_block_description')),
        ];
    }

    public function save(Request $request, Technology $technology): RedirectResponse
    {
        $request->validate([
            'technology.title' => [
                'required',
            ],
        ]);

        $technology->fill($request->get('technology'));
        $technology->save();

        Toast::info(__('admin.technology_saved'));

        return redirect()->route('platform.technologys');
    }

    public function remove(Technology $technology): RedirectResponse
    {
        $technology->delete();

        Toast::info(__('admin.technology_deleted'));

        return redirect()->route('platform.technologys');
    }
}
