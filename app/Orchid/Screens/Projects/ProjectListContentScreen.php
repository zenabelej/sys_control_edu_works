<?php

namespace App\Orchid\Screens\Projects;

use App\Models\Project;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;

class ProjectListContentScreen extends Screen
{
    public Project $project;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Project $project): iterable
    {
        $project->load('attachment');
        return [
            'project' => $project,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->project->title;
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('admin.go_back'))
                ->href(route('platform.projects')),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('project', [
                Sight::make(__('admin.name'))
                    ->render(fn(Project $project) => $project->title),
                Sight::make(__('admin.project_legend_event'))
                    ->render(fn(Project $project) => $project->event->title),
                Sight::make(__('admin.project_legend_event_start'))
                    ->render(fn(Project $project) => substr($project->start, 0, 10)),
                Sight::make(__('admin.project_legend_event_end'))
                    ->render(fn(Project $project) => substr($project->end, 0, 10)),
                Sight::make(__('admin.project_legend_difficulty'))
                    ->render(fn(Project $project) => $project->state->state),
                Sight::make(__('admin.project_legend_fixed_student'))
                    ->render(fn(Project $project) => $project->student?->name),
                Sight::make(__('admin.project_legend_technologies'))
                    ->render(fn(Project $project) => $project->technologys
                        ->map(fn($project) => $project->title)->implode(', ')),
                Sight::make(__('admin.project_legend_short_description'))
                    ->render(fn(Project $project) => $project->brief_description),
                Sight::make(__('admin.description'))
                    ->render(fn(Project $project) => $project->description),
                Sight::make(__('admin.project_legend_file'))
                    ->render(function (Project $project) {
                        return $project->attachment()->get()->map(function ($value) {
                            $url = env('APP_URL', 'http://controleduworks.uk') . $value->relativeUrl;
                            return "<a href='$url'>$value->original_name</a>";
                        })->implode('<br>');
                    }),
            ]),
        ];
    }
}
