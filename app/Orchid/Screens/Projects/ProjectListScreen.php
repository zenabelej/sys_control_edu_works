<?php

namespace App\Orchid\Screens\Projects;

use App\Models\Project;
use App\Orchid\Layouts\Projects\ProjectsListLayout;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class ProjectListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'project' => Project::filters()->paginate(10),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return __('admin.project_list_title');
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('admin.add'))
                ->icon('plus')
                ->href(route('platform.projects.create')),];
    }

    /**
     * The screen's layout elements.
     *
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            ProjectsListLayout::class,
        ];
    }

    public function remove($id): RedirectResponse
    {
        $project = Project::find($id);
        $project->delete();

        $project->technologys()->detach();

        $project->attachment()->each(function ($value) {
            $value->delete();
        });

        Toast::info(__('admin.project_deleted'));

        return redirect()->route('platform.projects');
    }
}
