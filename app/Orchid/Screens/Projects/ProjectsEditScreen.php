<?php

namespace App\Orchid\Screens\Projects;

use App\Models\Project;
use App\Orchid\Layouts\Projects\ProjectsEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class ProjectsEditScreen extends Screen
{

    public Project $project;
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Project $project): iterable
    {
        $project->load(['attachment', 'technologys']);
        return [
            'project' => $project,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return __('admin.project_title');
    }

    /**
     * The screen's action buttons.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('admin.save'))
                ->icon('check')
                ->method('save'),
            Button::make(__('admin.delete'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->project->exists),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            ProjectsEditLayout::class,
        ];
    }

    public function save(Request $request, Project $project): RedirectResponse
    {
        $request->validate([
            'project.title' => [
                'required',
                'max:255',
            ],
            'project.event_id' => [
                'required',
            ],
            'project.state_id' => [
                'required',
            ],
        ]);

        $data = [
            'title' => $request->input('project.title'),
            'event_id' => $request->input('project.event_id'),
            'state_id' => $request->input('project.state_id'),
            'brief_description' => $request->input('project.brief_description'),
            'description' => $request->input('project.description'),
            'students_id' => $request->input('project.students_id'),
        ];

        $start = $request->input('project.event.start');
        if ($start) {
            $data['start'] = $start;
        }

        $end = $request->input('project.event.end');
        if ($end) {
            $data['end'] = $end;
        }

        if ($project->exists) {
            $project->update($data);
        } else {
            $project->fill($data)->save();
        }

        $project->technologys()->sync($request->input('project.technologys'));

        $project->attachment()->syncWithoutDetaching(
            $request->input('project.attachment', [])
        );

        Toast::info(__('admin.project_saved'));

        return redirect()->route('platform.projects');
    }

    public function remove(Project $project): RedirectResponse
    {
        $project->delete();

        $project->technologys()->detach();

        $project->attachment()->each(function ($value) {
            $value->delete();
        });

        Toast::info(__('admin.project_deleted'));

        return redirect()->route('platform.projects');
    }
}
