<?php

namespace App\Orchid\Screens\Events;

use App\Models\Event;
use App\Orchid\Layouts\Events\EventsEditLayout;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class EventsEditScreen extends Screen
{

    public Event $event;

    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(Event $event): iterable
    {
        return [
            'event' => $event,
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return __('admin.event_title');
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('admin.save'))
                ->icon('check')
                ->method('save'),
            Button::make(__('admin.delete'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->event->exists),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::block(EventsEditLayout::class)
                ->title(__('admin.event_block_name'))
                ->description(__('admin.event_block_description')),
        ];
    }


    public function save(Request $request, Event $event): RedirectResponse
    {
        $request->validate([
            'event.title' => [
                'required',
                'max:255',
            ],
            'event.description' => [
                'max:255'
            ],
        ]);

        $event->fill($request->get('event'));
        $event->save();

        Toast::info(__('admin.event_saved'));

        return redirect()->route('platform.events');
    }

    public function remove(Event $event): RedirectResponse
    {
        $event->delete();

        Toast::info(__('admin.event_deleted'));

        return redirect()->route('platform.events');
    }
}
