<?php

namespace App\Notifications;

use App\Models\Project;
use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Orchid\Platform\Notifications\DashboardChannel;
use Orchid\Platform\Notifications\DashboardMessage;

class Apply extends Notification
{
    use Queueable;

    /**
     * @var Project
     */
    private Project $project;

    /**
     * Create a new notification instance.
     *
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return [DashboardChannel::class];
    }

    public function toDashboard($notifiable)
    {
        $student = Student::find(auth('student')->user()->getAuthIdentifier());
        return (new DashboardMessage)
            ->title("Подана заявка на проєкт \"{$this->project->title}\"")
            ->message("Користувач {$student->name} подав заяку на проєкт \"{$this->project->title}\".")
            ->action(route('platform.projects.edit', $this->project));
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
