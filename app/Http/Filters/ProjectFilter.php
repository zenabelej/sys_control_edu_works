<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class ProjectFilter extends AbstractFilter
{
    public const TITLE = 'title';
    public const BRIEF_DESCRIPTION = 'brief_description';
    public const SORT = 'sort';

    public function title(Builder $builder, $value)
    {
        $builder->where('title', 'ilike', "%{$value}%");
    }

    public function briefDescription(Builder $builder, $value)
    {
        $builder->where('brief_description', 'ilike', "%{$value}%");
    }

    public function sort(Builder $builder, $value)
    {
        $strStartsWith = str_starts_with($value, '-');
        if ($strStartsWith) {
            $value1 = substr($value, 1);
            $builder->orderBy($value1, 'desc');
        } else {
            $builder->orderBy($value);
        }
    }

    protected function getCallbacks(): array
    {
        return [
            self::TITLE => [$this, 'title'],
            self::BRIEF_DESCRIPTION => [$this, 'briefDescription'],
            self::SORT => [$this, 'sort'],
        ];
    }
}
