<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class AuthController extends Controller
{
    public function signIn(): Response
    {
        return Inertia::render('SignIn');
    }

    public function logout(): RedirectResponse
    {
        Auth::guard('student')->logout();
        return redirect()->route('home');
    }
}
