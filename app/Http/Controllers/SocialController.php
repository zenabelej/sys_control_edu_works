<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\User;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
     */
    public function redirectToGoogle(): \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * @return RedirectResponse|void
     */
    public function handleGoogleCallback()
    {
        try {
            $student = Socialite::driver('google')->user();

            $isStudent = $this->getCreateStudent($student);
            $isStudent->google_id = $student->getId();
            $isStudent->save();

            Auth::guard('student')->login($isStudent, true);

            return redirect()->intended('/');
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
     */
    public function redirectToGitHub(): \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * @return RedirectResponse|void
     */
    public function handleGitHubCallback()
    {
        try {
            $student = Socialite::driver('github')->user();

            $isStudent = $this->getCreateStudent($student);

            $isStudent->github_id = $student->getId();

            $isStudent->save();

            return redirect()->intended('/');
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
     */
    public function redirectToGitLab(): \Symfony\Component\HttpFoundation\RedirectResponse|RedirectResponse
    {
        return Socialite::driver('gitlab')->redirect();
    }

    /**
     * @return Application|RedirectResponse|Redirector|void
     */
    public function handleGitLabCallback()
    {
        try {
            $student = Socialite::driver('gitlab')->user();

            $isStudent = $this->getCreateStudent($student);

            $isStudent->gitlab_id = $student->getId();

            $isStudent->save();

            return redirect('/');
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * @param User $student
     * @return Student|Model
     */
    private function getCreateStudent(User $student): Model|Student
    {
        return Student::firstOrNew(['email' => $student->getEmail()], [
            'nickname' => $student->getNickname(),
            'name' => $student->getName(),
            'email' => $student->getEmail(),
            'avatar' => $student->getAvatar(),
        ]);
    }
}
