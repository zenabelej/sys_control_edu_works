<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Notifications\Apply;
use Illuminate\Support\Facades\Notification;
use Inertia\Inertia;
use Inertia\Response;
use Orchid\Platform\Models\Role;
use URL;

class NotificationController extends Controller
{
    public function notificationApply(Project $project): Response
    {
        $apply = new Apply($project);
        $users = Role::find(1)->getUsers();
        Notification::send($users, $apply);

        return Inertia::render('NotificationApply', ['back' => URL::previous()]);
    }
}
