<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Student;
use Illuminate\Database\Eloquent\Collection;
use Inertia\Inertia;
use Inertia\Response;

class StudentController extends Controller
{
    public function index(): Collection
    {
        return Student::all();
    }

    public function show(): Response
    {
        $user = auth('student')->user();
        $projects = Student::find($user->getAuthIdentifier())->withoutRelations()->projects->each(function (Project $pr) {
            $pr->event;
            $pr->technologys;
            $pr->state;
        });
        return Inertia::render('UserPage', [
            'user' => $user,
            'canLogin' => auth('student')->check(),
            'projects' => $projects,
        ]);
    }
}
