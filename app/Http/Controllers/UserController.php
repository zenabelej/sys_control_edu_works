<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    public function error(): Response
    {
        return Inertia::render('404');
    }
}
