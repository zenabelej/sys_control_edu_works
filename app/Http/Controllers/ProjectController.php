<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Inertia\Inertia;
use Inertia\Response;

class ProjectController extends Controller
{
    public function index(): Response
    {
        return Inertia::render('Project/Index', [
            'user' => auth('student')->user(),
            'canLogin' => auth('student')->check(),
        ]);
    }

    public function show(Project $project): Response
    {
        return Inertia::render('Project/Show', [
            'user' => auth('student')->user(),
            'canLogin' => auth('student')->check(),
            'project' => $project->load(['event', 'state', 'technologys']),
        ]);
    }
}
