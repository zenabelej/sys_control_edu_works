<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Filters\ProjectFilter;
use App\Http\Requests\Project\FilterRequest;
use App\Models\Project;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProjectController extends Controller
{
    /**
     * @param FilterRequest $request
     * @return LengthAwarePaginator
     * @throws BindingResolutionException
     */
    public function index(FilterRequest $request): LengthAwarePaginator
    {
        $data = $request->validated();

        $filter = app()->make(ProjectFilter::class, ['queryParams' => array_filter($data)]);

        $project = Project::filter($filter);

        return $project->with('event', 'state')->paginate(10);
    }
}
