<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @mixin IdeHelperStudent
 */
class Student extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nickname',
        'name',
        'email',
        'avatar',
        'google_id',
        'gitlab_id',
        'github_id',
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function projects(): HasMany
    {
        return $this->hasMany(Project::class, 'students_id');
    }
}
