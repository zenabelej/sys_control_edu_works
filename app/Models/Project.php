<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Filters\Types\Like;
use Orchid\Screen\AsSource;

/**
 * @mixin IdeHelperProject
 */
class Project extends Model
{
    use Filterable, AsSource, Attachable, HasFactory, SoftDeletes;
    use Traits\Filterable;

    protected $fillable = [
        'title',
        'start',
        'event_id',
        'end',
        'brief_description',
        'state_id',
        'students_id',
        'original_name_document',
        'document_file',
        'original_name_presentation',
        'presentation_file',
        'description',
        'created_at',
        'updated_at',
    ];

    protected array $allowedFilters = [
        'id' => Like::class,
        'title' => Like::class,
        'start' => Like::class,
        'event_id' => Like::class,
        'end' => Like::class,
        'brief_description' => Like::class,
        'state_id' => Like::class,
        'students_id' => Like::class,
        'original_name_document' => Like::class,
        'document_file' => Like::class,
        'original_name_presentation' => Like::class,
        'presentation_file' => Like::class,
        'description' => Like::class,
        'created_at' => Like::class,
        'updated_at' => Like::class,
        'deleted_at' => Like::class,
    ];

    protected array $allowedSorts = [
        'id',
        'title',
        'start',
        'event_id',
        'end',
        'brief_description',
        'state_id',
        'students_id',
        'original_name_document',
        'document_file',
        'original_name_presentation',
        'presentation_file',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'students_id');
    }

    public function technologys(): BelongsToMany
    {
        return $this->belongsToMany(Technology::class);
    }
}
