<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperState
 */
class State extends Model
{
    use HasFactory;

    protected $fillable = [
        'state',
    ];
}
