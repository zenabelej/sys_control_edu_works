<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * @mixin IdeHelperEvent
 */
class Event extends Model
{
    use Filterable, AsSource, HasFactory;

    protected $fillable = [
        'title',
        'description',
        'created_at',
        'updated_at',
    ];

    protected array $allowedFilters = [
        'id',
        'title',
        'description',
        'created_at',
        'updated_at',
    ];

    protected array $allowedSorts = [
        'id',
        'title',
        'description',
        'created_at',
        'updated_at',
    ];
}
