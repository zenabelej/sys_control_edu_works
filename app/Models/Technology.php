<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Filters\Types\Like;
use Orchid\Screen\AsSource;

/**
 * @mixin IdeHelperTechnology
 */
class Technology extends Model
{
    use Filterable, AsSource, HasFactory;

    protected $table = 'technologys';

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array|string[]
     */
    protected array $allowedFilters = [
        'id' => Like::class,
        'title' => Like::class,
        'created_at' => Like::class,
        'updated_at' => Like::class,
    ];

    /**
     * @var array|string[]
     */
    protected array $allowedSorts = [
        'id',
        'title',
        'updated_at',
        'created_at',
    ];

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
}
